//
//  AppDelegate.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//


import UIKit
import IQKeyboardManagerSwift


struct Page {
    var title: String
    var image: String
}


let PAGES = [
    [
        Page(title: "Extremely simple to use.", image: "1.png"),
        Page(title: "Getthe latest exchangerate for any FIAT or Cryptocurrency.", image: "2.png"),
        Page(title: "Easily customizable for your own needs.", image: "3.png"),
    ]
]

protocol Indexed {
    var index: Int! { get }
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application( _ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        IQKeyboardManager.shared.enable = true
        let vc = VerticalController(transitionStyle: .scroll, navigationOrientation: .vertical, options: nil)
        vc.count = PAGES.count
        vc.set(index: 0)
        
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = nav
        window!.backgroundColor = UIColor.white
        window!.makeKeyAndVisible()
        return true
        
    }
    
    
    
}
