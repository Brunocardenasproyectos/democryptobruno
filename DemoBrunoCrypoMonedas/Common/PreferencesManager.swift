//
//  PreferencesManager.swift
//  
//
//  Created by Bruno Cardenas Torres.
//  Copyright © 2022 . All rights reserved.
//

import Foundation

class PreferencesManager {
    

    private static let currencyFromKey = "currencyFrom"
   
    static var currencyFrom: String {
        get{
            if let valor = UserDefaults.standard.string(forKey: currencyFromKey) {
                return valor
            }else{
                return "PEN"
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: currencyFromKey)
        }
    }

    
}

