//
//  MngTabBarController.swift
//  MiFarma
//
//

import UIKit


class MngTabBarController: UITabBarController {

    var navVC1 = UIViewController()
    var navVC2 = UIViewController()
    var navVC3 = UIViewController()
    var navVC4 = UIViewController()
    var navVC5 = UIViewController()

    let tabIco1_off = UIImage(named: "inicioSelected")?.withRenderingMode(.alwaysOriginal)
    let tabIco1_on = UIImage(named: "inicioUnselected")?.withRenderingMode(.alwaysOriginal)
    
    let tabIco2_off = UIImage(named: "infoSelected")?.withRenderingMode(.alwaysOriginal)
    let tabIco2_on = UIImage(named: "infoUnselected")?.withRenderingMode(.alwaysOriginal)
    
    
    var tabIco3_off = UIImage(named:"operacionesSelected")?.withRenderingMode(.alwaysOriginal)
    var tabIco3_on = UIImage(named: "operacionesUnselected")?.withRenderingMode(.alwaysOriginal)

    
    var coordinator:MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()

    }

    func initUI() {


        self.coordinator = MainCoordinator(navigationController: UINavigationController())
        navVC1 = (self.coordinator?.fiatObject())!
        navVC1.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .selected)
        let bi1 = UITabBarItem(title: "Fiat", image: tabIco1_off, selectedImage: tabIco1_on)
        navVC1.tabBarItem = bi1

        self.coordinator = MainCoordinator(navigationController: UINavigationController())
        navVC3 = (self.coordinator?.cryptoObject())!
        navVC3.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .selected)
        let bi3 = UITabBarItem(title: "Crypto", image: tabIco2_off, selectedImage: tabIco2_on)
        navVC3.tabBarItem = bi3
        
        self.coordinator = MainCoordinator(navigationController: UINavigationController())
        navVC4 = (self.coordinator?.settingsObject())!
        navVC4.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .selected)
        let bi4 = UITabBarItem(title: "Settings", image: tabIco3_off, selectedImage: tabIco3_on)
        navVC4.tabBarItem = bi4

        self.viewControllers =  [navVC1, navVC3, navVC4]

        if let page = UserDefaults.standard.object(forKey: Constants.nameOptionsTabBar.INDEX) as? Int
        {
            self.selectedIndex = page
        }


    }
    


    
}
