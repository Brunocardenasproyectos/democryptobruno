//
//  OtherExtensions.swift
//  Cambix
//
//  Created by Bruno Cardenas on 14/06/21.
//
import Foundation
import UIKit

extension Dictionary {
    var prettyPrintedJSON: String? {
        do {
            let data: Data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(data: data, encoding: .utf8)
        } catch _ {
            return nil
        }
    }
}
extension UIApplication {
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}
extension  UIViewController {

    func showAlert(withMessage message:String) {
        let alert = UIAlertController(title: "Demo crypto Currency", message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
    
        alert.addAction(ok)

        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}


enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    func base64(format: ImageFormat) -> String? {
        var imageData: Data?

        switch format {
        case .png: imageData = self.pngData()
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }

        return imageData?.base64EncodedString()
    }
}

extension String {
    func imageFromBase64() -> UIImage? {
        guard let data = Data(base64Encoded: self) else { return nil }

        return UIImage(data: data)
    }
}
extension UITextField {


/**
this function adds a right view on the text field
*/


func addRightView(rightView: String, tintColor: UIColor? = nil) {
    if rightView != "" {
        let rightview = UIButton(type: .custom)
        
        if tintColor != nil {
            let templateImage = UIImage(named: rightView)?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            rightview.setImage(templateImage, for: .normal)
            rightview.tintColor = tintColor
        }
        else{
            rightview.setImage(UIImage(named: rightView), for: .normal)
        }
        

        
        self.rightViewMode = .always
        self.rightView = rightview
    }
    else{
        self.rightView = .none
        for vw in self.subviews where vw.tag == 1000 {
            vw.removeFromSuperview()
        }
    }
}

}
