//
//  FilesManager.swift
//  HablaQuechua
//
//  Created by pruebaa on 12/21/19.
//  Copyright © 2019 Bruno Cardenas. All rights reserved.
//

import UIKit


 class FilesManager {
    enum Error: Swift.Error {
        case fileAlreadyExists
        case invalidDirectory
        case writtingFailed
    }
    let fileManager: FileManager
    
    static let shared = FilesManager()
    
    init(fileManager: FileManager = .default) {
        self.fileManager = fileManager
    }
    func save(fileNamed: String, data: Data) throws {
        guard let url = makeURL(forFileNamed: fileNamed) else {
            throw Error.invalidDirectory
        }
        do {
            try data.write(to: url)
        } catch {
            debugPrint(error)
            throw Error.writtingFailed
        }
    }
    private func makeURL(forFileNamed fileName: String) -> URL? {
        guard let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        return url.appendingPathComponent(fileName)
    }
    
    func read(fileNamed: String) throws -> Data {
        guard let url = makeURL(forFileNamed: fileNamed) else {
            throw Error.invalidDirectory
        }
//        guard fileManager.fileExists(atPath: url.absoluteString) else {
//            throw Error.invalidDirectory
//        }
        do {
            return try Data(contentsOf: url)
        } catch {
            debugPrint(error)
            throw Error.invalidDirectory
        }
    }
    
}
