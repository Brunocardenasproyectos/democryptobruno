//
//  Constants.swift
//  MiFarma
//
//

import Foundation
import UIKit

class Constants {


    public static var urlBase = "https://v6.exchangerate-api.com/v6/"
    public static let apiKey = "a22cd38cda7637138614f215"
    public static let getConversionRate = urlBase + apiKey + "/pair/"
   
    public static let cornerValue = 15

    struct nameOptionsTabBar {
        static let INDEX = "indexinicial"
    }
    struct networking {
        static let success = "success"
    }
    struct currencys {
        static let arrayCurrencys = ["EUR","USD","PEN","MXN","CAD","CNY"]
    }

}
