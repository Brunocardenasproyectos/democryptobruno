//
//  MainCoordinator.swift
//  PagosDigitales
//
//  Created by miguel tomairo on 5/29/20.
//  Copyright © 2020 Banco de Comercio. All rights reserved.
//

import Foundation
import UIKit

protocol MainCoordinatorDelegate: AnyObject {
    func passData(_ value: String)
    func noPassData()
}

class MainCoordinator: Coordinator {

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var delegate: MainCoordinatorDelegate?
    
    init()
    {
        self.navigationController = UINavigationController()
    }
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func fiatObject() -> UIViewController {
        navigationController.setNavigationBarHidden(true, animated: false)
        let service = FiatService()
        let viewModel = FiatViewModel(servicee: service)
        let vc = FiatView.instantiate("FiatView")
        vc.vm = viewModel
        vc.coordinator = self
        navigationController.viewControllers = [vc]
        return navigationController
    }
    
    func cryptoObject() -> UIViewController {
        navigationController.setNavigationBarHidden(true, animated: false)
        let vc = CryptoView.instantiate("CryptoView")
        vc.coordinator = self
        navigationController.viewControllers = [vc]
        return navigationController
    }
    
    func settingsObject() -> UIViewController {
        navigationController.setNavigationBarHidden(true, animated: false)
        let vc = SettingsView.instantiate("SettingsView")
        vc.coordinator = self
        navigationController.viewControllers = [vc]
        return navigationController
    }
    
    
    func goToRootTabBar()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        UserDefaults.standard.set(0, forKey: Constants.nameOptionsTabBar.INDEX)
        appDelegate.window?.rootViewController = MngTabBarController()
    }
  
    
}
