//
//  Coordinator.swift
//  PagosDigitales
//
//  Created by miguel tomairo on 5/29/20.
//  Copyright © 2020 Banco de Comercio. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator {
    
    var childCoordinators: [Coordinator] {get set}
    var navigationController: UINavigationController {get set}
}

