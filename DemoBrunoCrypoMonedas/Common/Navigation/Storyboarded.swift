//
//  Storyboarded.swift
//  PagosDigitales
//
//  Created by miguel tomairo on 5/29/20.
//  Copyright © 2020 Banco de Comercio. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded {
    
    static func instantiate() -> Self
}

extension Storyboarded where Self: UIViewController {
    
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
    
    static func instantiate(_ sb: String) -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: sb, bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
}
