//
//  NetworkManager.swift
//  NetworkManager
//
//  Created by Rinto Andrews on 25/07/19.
//  Copyright © 2019 Rinto Andrews. All rights reserved.
//

import Foundation

enum HttpMethod:String{
    case get = "get"
    case post = "post"
}
class NetworkManager {
    
    static let shared = NetworkManager()
    
    func dataTask(serviceURL:String,httpMethod:HttpMethod,parameters:[String:Any]?,completion:@escaping (AnyObject?, banError?) -> Void) -> Void {
       
        requestResource(serviceURL: serviceURL, httpMethod: httpMethod, parameters: parameters, completion: completion)
    }
    
    private func requestResource(serviceURL:String,httpMethod:HttpMethod,parameters:[String:Any]?,completion:@escaping (AnyObject?, banError?) -> Void) -> Void {
        
        var request = URLRequest(url: URL(string:"\(serviceURL)")!)
       
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = httpMethod.rawValue
        
        if (parameters != nil) {
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
        }
        
        let sessionTask = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
            
            if (data != nil){
                let result = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                if result == nil
                {
                    completion (nil,banError(message: "Json error"))
                }
                else
                {
                    completion (data as AnyObject, nil)
                }
            }
                
            if (error != nil) {
                completion (nil,banError(message: error?.localizedDescription))
            }
        }
        sessionTask.resume()
    }
}
