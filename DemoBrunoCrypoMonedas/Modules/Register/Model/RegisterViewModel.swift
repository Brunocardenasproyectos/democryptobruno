//
//  RegisterViewModel.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import Foundation
import EasyStash


protocol RegisterViewModelDelegate
{
    func getSuccesfull()

}

class RegisterViewModel
{
    var storage: Storage? = nil
    var options: Options = Options()
    var delegate:RegisterViewModelDelegate?
    init() {
        options.folder = "Users"
        storage = try? Storage(options: options)
    }
    

    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    func addNewUser(user:User)
    {
        var users = try? storage!.load(forKey: "users", as: [User].self)
        if users == nil
        {
            try? storage!.save(object: [user], forKey: "users")
        }
        else
        {
            users?.append(user)
            try? storage!.save(object: users, forKey: "users")
        }
        
        delegate?.getSuccesfull()
    }
}
