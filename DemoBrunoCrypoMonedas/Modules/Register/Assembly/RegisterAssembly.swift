//
//  LoginAssembly.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import Foundation
import UIKit


class RegisterAssembly
{
    func buildVC() -> UIViewController
    {
        let vm = RegisterViewModel()
        let vc = UIStoryboard(name: "RegisterView", bundle: nil).instantiateViewController(withIdentifier: "RegisterView") as! RegisterView
        vc.vm = vm
        return vc
    }
}
