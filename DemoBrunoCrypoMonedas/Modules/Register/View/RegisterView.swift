//
//  RegisterView.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import UIKit

class RegisterView: UIViewController {

    @IBOutlet weak var btnCreate: UIButton!
    @IBOutlet weak var txtRepeatPass: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    var ValidName = false
    var validEmail = false
    var passEqualNotEmpty = false
    var vm:RegisterViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vm?.delegate = self
        txtName.addTarget(self, action: #selector(nameTextChange(textField:)), for: .editingChanged)
        txtEmail.addTarget(self, action: #selector(mailTextChange(textField:)), for: .editingChanged)
        txtPassword.addTarget(self, action: #selector(passwordTextChange(textField:)), for: .editingChanged)
        txtRepeatPass.addTarget(self, action: #selector(passwordTextChange(textField:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc final private func nameTextChange(textField: UITextField) {

        self.ValidName =  textField.text!.contains(" ")

        if !self.ValidName
        {
            self.txtName.addRightView(rightView: "errorIcon", tintColor: .red)
        }
        else
        {
            self.txtName.addRightView(rightView: "", tintColor: .red)
        }
        
        self.isValidData()
        
    }
    @objc final private func mailTextChange(textField: UITextField) {
        self.validEmail = self.vm!.isValidEmail(textField.text!)
        if self.validEmail
        {
            self.txtEmail.addRightView(rightView: "", tintColor: .red)
        }
        else
        {
            self.txtEmail.addRightView(rightView: "errorIcon", tintColor: .red)
        }
        
        self.isValidData()
    }
    @objc final private func passwordTextChange(textField: UITextField) {
        
        if txtPassword.text!.isEmpty
        {
            self.passEqualNotEmpty = false
           
        }
        self.passEqualNotEmpty = txtPassword.text! == txtRepeatPass.text!
        
        if self.passEqualNotEmpty
        {
            self.txtPassword.addRightView(rightView: "", tintColor: .red)
        }
        else
        {
            self.txtPassword.addRightView(rightView: "errorIcon", tintColor: .red)
        }
        
        self.isValidData()
    }
    
    
    func isValidData()
    {
        if ValidName && validEmail && passEqualNotEmpty
        {
            btnCreate.isEnabled = true
        }
        else
        {
            btnCreate.isEnabled = false
        }
    }
   
    @IBAction func onTapCancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapCreate(_ sender: Any) {
        
        self.vm?.addNewUser(user: User(name: txtName.text!, email: txtEmail.text!, pass: txtPassword.text!))
        
    }
}

extension RegisterView: RegisterViewModelDelegate
{
    func getSuccesfull() {
        let alert = UIAlertController(title: "Demo crypto Currency", message: "User Added", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.navigationController?.popViewController(animated: true)
        })

        alert.addAction(ok)
        self.present(alert, animated: true)
    }
    
   
}
