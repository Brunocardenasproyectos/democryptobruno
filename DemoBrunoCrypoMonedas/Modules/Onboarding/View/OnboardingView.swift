//
//  OnboardingView.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import UIKit
// MARK: PageController
class PageController: UIPageViewController, Indexed {
    var index: Int! = -1
    var count: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
    }
    
    func page(index: Int) -> UIViewController? {
        return nil
    }
    
    func atIndex(index: Int) -> UIViewController? {
        if (index >= 0) && (index < self.count) {
            return self.page(index: index)
        }
        return nil
    }
    
    func set(index: Int) {
        let animate = self.viewControllers!.count > 0
        var update = !animate
        var dir: UIPageViewController.NavigationDirection = .forward
        if animate {
            if let ch = self.viewControllers?[0] as? Indexed {
                update = ch.index != index
                dir = index > ch.index ? .forward : .reverse
            }
        }
        if update {
            if let vc = self.atIndex(index: index) {
                self.setViewControllers([vc], direction: dir, animated: animate, completion: nil)
            }
        }
    }
}

// MARK: PageController: UIPageViewControllerDelegate
extension PageController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let child = pendingViewControllers[0] as? PageController {
            child.set(index: 0)
        }
    }
}

// MARK: PageController: UIPageViewControllerDataSource
extension PageController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? Indexed else { return nil }
        return self.atIndex(index: vc.index - 1)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? Indexed else { return nil }
        return self.atIndex(index: vc.index + 1)
    }
    
}


// MARK: VerticalController
class VerticalController: PageController {
    override func page(index: Int) -> UIViewController? {
        let hc = HorizontController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        hc.count = PAGES[index].count
        hc.index = index
        hc.set(index: 0)
        return hc
    }
}

// MARK: HorizontController
class HorizontController: PageController {
    override func page(index: Int) -> UIViewController? {
        let cc = ContentController()
        cc.page = PAGES[self.index][index]
        cc.index = index
        return cc
    }
}

// MARK: ContentController
class ContentController: UIViewController, Indexed {
    var index: Int!
    var page: Page!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let lb = UILabel()
        lb.textAlignment = .center
        lb.numberOfLines = 0
        lb.sizeToFit()
        lb.text = self.page.title
        lb.font = UIFont.systemFont(ofSize: 16)
        let widthConstraint = NSLayoutConstraint(item: lb, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 250)
        let heightConstraint = NSLayoutConstraint(item: lb, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 150)
        lb.addConstraint(widthConstraint)
        lb.addConstraint(heightConstraint)
        
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.image = UIImage(named: page.image)
        
        
        let views = [
            "iv": iv,
            "lb": lb,
        ]
        for (_, v) in views {
            v.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(v)
        }
        NSLayoutConstraint.activate(
            // NSLayoutConstraint.constraints(withVisualFormat: "H:|-[lb]-|", options: .alignAllCenterX, metrics: [:], views: views) +
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[iv]-|", options: .alignAllCenterX, metrics: [:], views: views) +
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[lb(<=16)]-[iv]-30-|", options: .alignAllCenterX, metrics: [:], views: views)
        )
        

        let closeButton = UIButton()
        closeButton.setTitle("Close", for: .normal)
        closeButton.setTitleColor(.systemBlue, for: .normal)
        closeButton.frame = CGRect(x: 20, y: 35, width: 50, height: 40)
        closeButton.addTarget(self, action: #selector(pressed), for: .touchUpInside)
        
        self.view.addSubview(closeButton)
    }
    
    @objc func pressed() {

        let vc = LoginAssembly().buildVC()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }



}

