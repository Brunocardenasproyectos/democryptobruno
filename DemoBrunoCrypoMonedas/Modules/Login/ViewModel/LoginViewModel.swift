//
//  LoginViewModel.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import Foundation
import EasyStash



protocol LoginViewModelDelegate
{
    func getSuccesfull()
    func getError(errorMessage:String)
}

class LoginViewModel
{
    
    var delegate:LoginViewModelDelegate?
    var storage: Storage? = nil
    var options: Options = Options()
    
    init() {
        options.folder = "Users"
        storage = try? Storage(options: options)
    }
    
    
    func checkUser(user:User)
    {
        
        let arrUsers = try? storage!.load(forKey: "users", as: [User].self)
        if arrUsers == nil
        {
            self.delegate?.getError(errorMessage: "No users")
            return
        }
        else
        {
            var existe = false
            for a in arrUsers!
            {
                if a.email == user.email && a.pass == user.pass
                {
                    existe = true
                }

            }
            
            if existe
            {
                self.delegate?.getSuccesfull()
            }
            else
            {
                self.delegate?.getError(errorMessage: "Invalid credentials")
            }
        }
        
        
        
    }
    
    
}

