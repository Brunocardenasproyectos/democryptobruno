//
//  LoginAssembly.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import Foundation
import UIKit


class LoginAssembly
{
    func buildVC() -> UIViewController
    {
     
        let vm = LoginViewModel()
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginView") as! LoginView
        vc.vm = vm
        return vc
    }
}
