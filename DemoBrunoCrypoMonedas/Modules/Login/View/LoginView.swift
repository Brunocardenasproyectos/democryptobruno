//
//  Login.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import UIKit

class LoginView: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    var vm:LoginViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        vm?.delegate = self
    }
    
    @IBAction func onTapCreate(_ sender: Any) {
        
        let vc = RegisterAssembly().buildVC()
        self.navigationController?.pushViewController(vc, animated:true )
    }
    func isValidData() -> Bool
    {
        if txtEmail.text!.isEmpty || txtPassword.text!.isEmpty
        {
            return false
        }
        else
        {
            return true
        }
    }
    @IBAction func onTapLogin(_ sender: Any) {
        
        if isValidData()
        {
            
            self.vm?.checkUser(user: User(name: "", email: txtEmail.text!, pass: txtPassword.text!))
        }
        else
        {
            self.showAlert(withMessage: "Complete all fields")
        }
    }
}

extension LoginView: LoginViewModelDelegate
{
    
    func getSuccesfull() {
        MainCoordinator().goToRootTabBar()
    }
    
    func getError(errorMessage: String) {
        self.showAlert(withMessage: errorMessage)
    }
    
    
}
