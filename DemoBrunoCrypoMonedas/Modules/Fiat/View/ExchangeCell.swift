//
//  ExchangeCell.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 16/01/22.
//

import UIKit

class ExchangeCell: UITableViewCell {

    @IBOutlet weak var txtResult: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblExchangeRate: UILabel!
    @IBOutlet weak var lblCurrencyTo: UILabel!
    var obj:FiatResponse?
    override func awakeFromNib() {
        super.awakeFromNib()
        txtAmount.addTarget(self, action: #selector(amountChanged(textField:)), for: .editingChanged)
    }
    func fillData()
    {
        lblCurrencyTo.text = obj?.targetCode
        lblExchangeRate.text = "\(obj?.conversionRate ?? 0.0)"
    }
    @objc final private func amountChanged(textField: UITextField) {
        
        if !textField.text!.isEmpty && obj != nil
        {
            let amount = Double(textField.text!)!
            let rate = obj!.conversionRate!
            let result = amount * rate
            txtResult.text = "\(result)"
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
