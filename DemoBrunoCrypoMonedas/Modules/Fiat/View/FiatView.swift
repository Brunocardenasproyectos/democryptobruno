//
//  FiatView.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import UIKit

class FiatView: UIViewController, Storyboarded {

    @IBOutlet weak var lblCurrencyFrom: UILabel!
    weak var coordinator: MainCoordinator?
    @IBOutlet weak var tb: UITableView!
    var vm:FiatViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        self.vm?.delegate = self
        fillData()
        getData()
    }

    func fillData()
    {
        lblCurrencyFrom.text = PreferencesManager.currencyFrom
    }
    
    func getData()
    {
        for a in Constants.currencys.arrayCurrencys
        {
            self.vm?.checkUser(currencyFrom: PreferencesManager.currencyFrom, currencyTo: a)
        }
    }

}

extension FiatView: FiatViewModelDelegate
{
    func getSuccesfullResponse(fiatObject: FiatResponse) {
        self.vm?.fiatObjects.append(fiatObject)
        
        DispatchQueue.main.async {
            self.tb.reloadData()
        }
        
    }
    

    func getErrorResponse(errorMessage: String) {
        self.showAlert(withMessage: errorMessage)
    }
 
}

extension FiatView: UITableViewDataSource, UITableViewDelegate
{

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExchangeCell", for: indexPath) as? ExchangeCell else  {
            fatalError("error guard indexPath")
        }
        let obj = self.vm!.fiatObjects[indexPath.row]
        cell.obj = obj
        cell.fillData()
        
        return cell
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.vm?.fiatObjects.count ?? 0
    }
}
