//
//  FiatResponse.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 16/01/22.
//

import Foundation

// MARK: - Welcome
struct FiatResponse: Codable {
    var result: String?
    var documentation, termsOfUse: String?
    var timeLastUpdateUnix: Int?
    var timeLastUpdateUTC: String?
    var timeNextUpdateUnix: Int?
    var timeNextUpdateUTC, baseCode, targetCode: String?
    var conversionRate: Double?

    enum CodingKeys: String, CodingKey {
        case result, documentation
        case termsOfUse = "terms_of_use"
        case timeLastUpdateUnix = "time_last_update_unix"
        case timeLastUpdateUTC = "time_last_update_utc"
        case timeNextUpdateUnix = "time_next_update_unix"
        case timeNextUpdateUTC = "time_next_update_utc"
        case baseCode = "base_code"
        case targetCode = "target_code"
        case conversionRate = "conversion_rate"
    }
}

