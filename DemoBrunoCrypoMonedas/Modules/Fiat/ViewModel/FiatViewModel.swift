//
//  FiatViewModel.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 16/01/22.
//

import Foundation

import SVProgressHUD


protocol FiatViewModelDelegate
{
    func getSuccesfullResponse(fiatObject:FiatResponse)
    func getErrorResponse(errorMessage:String)
}

class FiatViewModel
{
    var service:FiatService?
    var delegate:FiatViewModelDelegate?
    var fiatObjects = Array<FiatResponse>()
    init(servicee:FiatService)
    {
        self.service = servicee
    }
    
    func checkUser(currencyFrom:String,currencyTo:String)
    {
        SVProgressHUD.show()
        self.service?.getExchangeRate(currencyFrom: currencyFrom, currencyTo: currencyTo, completion: { response, error in
            SVProgressHUD.dismiss()
            if response != nil
            {
                if response!.result != Constants.networking.success
                {
                    self.delegate?.getErrorResponse(errorMessage: (response?.result)!)
                }
                else
                {
                    self.delegate?.getSuccesfullResponse(fiatObject: response!)
                }
            }
            else
            {
                self.delegate?.getErrorResponse(errorMessage: error?.message ?? "Error desconocido")
            }
            
        })
        
    }
    
    
}
