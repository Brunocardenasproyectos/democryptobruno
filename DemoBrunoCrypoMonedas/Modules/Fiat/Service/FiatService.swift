//
//  ceduleService.swift
//  brtest
//
//  Created by Bruno on 20/12/21.
//

import Foundation

class FiatService
{
    func getExchangeRate(currencyFrom:String,currencyTo:String,completion:@escaping (FiatResponse?, banError?) -> Void) -> Void {
        
        let url = Constants.getConversionRate + currencyFrom + "/" + currencyTo
        
        NetworkManager.shared.dataTask(serviceURL: url, httpMethod: .get, parameters: nil) { response, error in

            if response != nil
            {
                let result = try? JSONDecoder().decode(FiatResponse.self, from: response as! Data)
                if (result != nil){
                    completion (result, nil)
                }
            }
            else
            {
                completion (nil,error!)
            }

            
        }
    }
}
