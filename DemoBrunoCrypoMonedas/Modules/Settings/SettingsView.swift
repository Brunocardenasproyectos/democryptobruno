//
//  SettingsView.swift
//  DemoBrunoCrypoMonedas
//
//  Created by bruno cardenas on 15/01/22.
//

import UIKit

class SettingsView: UIViewController, Storyboarded{
    
    @IBOutlet weak var txtCurrencys: UITextField!
    weak var coordinator: MainCoordinator?
    let picker = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        fillData()
        configure()
        
    }
    
    func configure()
    {
        picker.delegate = self
        self.txtCurrencys.inputView = self.picker
    }
    func fillData()
    {
        txtCurrencys.text = PreferencesManager.currencyFrom
    }
    
    @IBAction func onTapSave(_ sender: Any) {
        PreferencesManager.currencyFrom = txtCurrencys.text!
        showAlert(withMessage: "Main base currency Updated!")
    }
    
}

extension SettingsView: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Constants.currencys.arrayCurrencys.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let item = Constants.currencys.arrayCurrencys[row]
        return item
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let item = Constants.currencys.arrayCurrencys[row]
        self.txtCurrencys.text = item
        
    }
}
